
cc.Class({
    extends: cc.Component,

    properties: {
        animation: {
            default: null,
            type: cc.Animation,
        },

        scoreLabel: {
            default: null,
            type: cc.Label,
        },
    },

    onLoad: function() {
        this.animation.on('finished', this.onPlayFinished, this);
    },

    init: function(mgr) {
        this.mgr = mgr;
    },

    play: function() {
        this.animation.play('score_pop');
    },

    setScore: function(score) {
        this.scoreLabel.string = "+" + score;
    },

    onPlayFinished: function(type, state) {
        this.mgr.despawnScoreFX(this);
    },
});
