use simplebase::engine::*;

pub fn init() {
    let database = new_empty_database();
    database.save_database("data.txt");
}
